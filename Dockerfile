FROM python:3.7

RUN mkdir /app
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY src . 

RUN pyarmor --silent obfuscate --output obfuscated main.py
RUN /bin/rm /app/requirements.txt /app/main.py /bin/bash /bin/dash
WORKDIR /app/obfuscated

CMD ["uvicorn", "main:app", "--reload", "--log-level", "critical", "--host", "0.0.0.0"]
